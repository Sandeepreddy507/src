import { Component, OnInit } from '@angular/core';
import { AppServiceService } from 'src/app/services/app-service.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-petslist',
  templateUrl: './petslist.component.html',
  styleUrls: ['./petslist.component.css']
})
export class PetslistComponent implements OnInit {
  selecteduserdata:null;
  pets: any  [];
 
  submitted:boolean=true;
  isAdd:boolean=false;
  baseurl: string = `${environment.baseurl}/pets`;

  constructor(private service: AppServiceService) { }

  ngOnInit(): void {
  }



  getPetdata=()=>{
    this.service.getData(this.baseurl).subscribe((data:any[]) => {
      this.pets=data;
      return JSON.stringify(data) ;
    }, (error) => {
      console.log(error);

    })
  }
}
