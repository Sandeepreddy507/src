import { Component, OnInit } from '@angular/core';
import { AppServiceService } from 'src/app/services/app-service.service';
import { environment } from 'src/environments/environment';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs';
import { Pets } from 'src/app/pets';

@Component({
  selector: 'app-pet-data',
  templateUrl: './pet-data.component.html',
  styleUrls: ['./pet-data.component.css']
})
export class PetDataComponent implements OnInit {
  selecteduserdata:null;
  pets: any  [];
  postdata:NgForm;
  submitted:boolean=true;
  isAdd:boolean=false;
  
  selectedata: any ={
    petName:'',
     petOwner:'',

  }
  baseurl: string = `${environment.baseurl}/pets`;

  constructor(private service: AppServiceService,) { }

  ngOnInit(): void {
  
  }



  delatePetData = (id) => {
    this.service.deleteData(`${this.baseurl}/${id}`).subscribe((response) => {
     
      console.log(response);

    });
  }

  addPet = () => {
    this.submitted = true;
    this.service.postData(`${this.baseurl}`, this.selectedata).subscribe((response) => {
      console.log(response);
      this.getPetdata();
    },
      (error) => {
        console.log(error);
      },
      () => {
 
      })
  }
 
  onSelect = (pet) => {
    this.selectedata= pet;
    this.isAdd = false;
      }

      submit = (petRef) => {
        this.submitted = true;
        if (petRef.valid) {
        if (this.isAdd) {
        this.postPetData();
              }
        else {
        this.updatePetData();
              }
            }
          }

  getPetdata=()=>{
    this.service.getData(this.baseurl).subscribe((data:any[]) => {
      this.pets=data;
      return JSON.stringify(data) ;
    }, (error) => {
      console.log(error);

    })
  }
  postPetData = () => {
    let body = "&petName=" + this.selectedata.petName + "&petOwner=" + this.selectedata.petOwner;
    this.service.postData(this.baseurl, this.postdata.value).subscribe((response) => {
      console.log(response);
     
    }, (error) => {
      console.log(error);

    });
  }




  updatePetData = () => {
    this.service.updateData(`${this.baseurl}/${this.selectedata.id}`, this.selectedata).
    subscribe((response) => {
      console.log(response);

    }, (error) => {
      console.log(error);

    });
  }





}
