import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { PetDataComponent } from './pets/pet-data/pet-data.component';
import { RegisterComponent } from './register/register/register.component';
import { LoginComponent } from './login/login/login.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HomeComponent } from './home/home/home.component';
import { RouterModule, Routes } from '@angular/router';
import { CustomComponent } from './directives/custom/custom.component';
import { PetslistComponent } from './ListPets/petslist/petslist.component';
import { AddpetsComponent } from './AddPets/addpets/addpets.component';





@NgModule({
  declarations: [
    AppComponent,
    PetDataComponent,
    RegisterComponent,
    LoginComponent,
    HomeComponent,
    CustomComponent,
    PetslistComponent,
    AddpetsComponent,
  
  
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule ,
    FormsModule,


  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
