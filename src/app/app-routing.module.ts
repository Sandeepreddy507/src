import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login/login.component';
import { RegisterComponent } from './register/register/register.component';
import { PetDataComponent } from './pets/pet-data/pet-data.component';
import { PetslistComponent } from './ListPets/petslist/petslist.component';
import { AddpetsComponent } from './AddPets/addpets/addpets.component';


const routes: Routes = [
 
   
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'pets', component: PetDataComponent },
    {path:'petlist',component:PetslistComponent},
    {path:'addpet',component:AddpetsComponent},
    { path: '', redirectTo:'',pathMatch:"full" },

   
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {


  
 }
