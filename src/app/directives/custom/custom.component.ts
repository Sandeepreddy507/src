import { Component, OnInit, HostListener, ElementRef } from '@angular/core';

@Component({
  selector: 'app-custom',
  templateUrl: './custom.component.html',
  styleUrls: ['./custom.component.css']
})
export class CustomComponent implements OnInit {

  constructor(private elRef: ElementRef) { 
  }
  @HostListener('mouseover') onMouseOver() {
    this.changeColor('yellow');
  }
  @HostListener('mouseleave') onMouseLeave() {
    this.changeColor('lime');
  }
  private changeColor(color: string) {
    this.elRef.nativeElement.style.color = color;
  } 

  ngOnInit(): void {
  }

}
