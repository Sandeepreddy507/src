import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Users } from '../users';

@Injectable({
  providedIn: 'root'
})
export class AppServiceService {
 baseurl: string = `${environment.baseurl}/pets`;
 baseurl1: string = `${environment.baseurl}/users`;
  constructor(private http: HttpClient) {


  }
  getData = (url: string) => {
    return this.http.get(url);
  }

  deleteData = (id) => {
    return this.http.delete(id);

  }

  postData(url, postobj) {

    return this.http.post(url, postobj);

  }

  updateData(url,postObj){
      return this.http.put(url,postObj);
  }


  getAll() {
    return this.http.get<Users[]>(`${this.baseurl1}/users`);
}

getById(id: number) {
    return this.http.get(`${this.baseurl1}/users` + id);
}

register(url,user: Users) {
    return this.http.post(url, user);
}

update(user: Users) {
    return this.http.put(`${this.baseurl1}}/users/` + user.id, user);
}



}

