import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { environment } from 'src/environments/environment';
import { AppServiceService } from 'src/app/services/app-service.service';

@Component({
  selector: 'app-addpets',
  templateUrl: './addpets.component.html',
  styleUrls: ['./addpets.component.css']
})
export class AddpetsComponent implements OnInit {
  constructor(private service: AppServiceService) { }

  ngOnInit(): void {
  }

  selecteduserdata:null;
  pets: any  [];
  postdata:NgForm;
  submitted:boolean=true;
  isAdd:boolean=false;

  selectedata: any ={
    petName:'',
     petOwner:'',

  }
  baseurl: string = `${environment.baseurl}/pets`;
  postPetData = () => {
    let body = "&petName=" + this.selectedata.petName + "&petOwner=" + this.selectedata.petOwner;
    this.service.postData(this.baseurl, this.postdata.value).subscribe((response) => {
      console.log(response);
     
    }, (error) => {
      console.log(error);

    });
  }


  submit = (petRef) => {
    this.submitted = true;
    if (petRef.valid) {
    if (this.isAdd) {
    this.postPetData();
          }
        }
      }
      

}
