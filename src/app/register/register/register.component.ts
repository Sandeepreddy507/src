import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AppServiceService } from 'src/app/services/app-service.service';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  
  mobilepattern:"^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$";
  mailpattern:"\w+([-+.]\w+)*@(dbs\.com|hcl\.com)";
  baseurl1: string = `${environment.baseurl}/users`;

  profileForm:FormGroup;
  issubmitted:boolean=false;
  loading:boolean=true;
  constructor(private service:AppServiceService,private router:Router) { }

 

  onSubmit() {
  this.issubmitted=true;
  if(this.profileForm.valid){
    return;
    console.warn(this.profileForm.value);
  }
  this.loading = true;
        this.service.register(this.baseurl1,this.profileForm.value).subscribe(
                data => {
                    window.alert('Registration successful');
                      this.router.navigate(['/login']);
                },
                error => {
                  window.alert (error);
                    this.loading = false;
                })
         
              
        
}



 
  

  ngOnInit(): void {

    this.profileForm = new FormGroup({
      firstName: new FormControl('',Validators.required),
      lastName: new FormControl('',Validators.required),
      emailId: new FormControl('',[Validators.required,Validators.pattern(this.mailpattern)]),
      mobileNumber:new FormControl('',[Validators.required,Validators.pattern(this.mobilepattern)]),
      adress:new FormControl('',[Validators.required,Validators.maxLength(10)]),
      gender:new FormControl('',Validators.required),
     password:new FormControl('',Validators.required),
    });
  }window

}




